package com.soma.scrapbook;

/**
 * @author Mahendra Prajapati
 * @since 08-08-2020
 */
public class ClassParent {

    private ChildClass childClass;

    public ClassParent(ChildClass childClass) {
        this.childClass = childClass;
    }

    public int parentMethod() {
        try {
            childClass.childMethod();
        } catch (Exception e) {
            new RuntimeException(e);
        }
        return 0;
    }

}
