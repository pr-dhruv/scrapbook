package com.soma.scrapbook;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import static org.junit.Assert.assertNotSame;
import static org.junit.Assert.assertSame;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

/**
 * @author Mahendra Prajapati
 * @since 08-08-2020
 */
public class ClassParentTest {

    @Mock
    private ChildClass childClass;

    private ClassParent parent;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        parent = new ClassParent(childClass);
    }

    @Test
    public void useChildVoidMethod() throws Exception {
        assertNotSame(1, parent.parentMethod());
        verify(childClass, times(1)).childMethod();
    }

    @Test
    public void useChildVoidMethod1() throws Exception {
        assertSame(0, parent.parentMethod());
        verify(childClass, times(1)).childMethod();
    }

    @After
    public void tearDown() {
        parent = null;
    }

}